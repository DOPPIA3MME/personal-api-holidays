//
//  ViewController.swift
//  API Holidays
//
//  Created by Maurizio Minieri on 21/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UITabBarDelegate {
	
	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var segmentedControl: UISegmentedControl!
	@IBOutlet weak var collectionView: UICollectionView!
	
	
	var holidayTitle: String!
	var holidayDescription: String!
	
	
	//Se facessi una semplice dichiarazione myHolidays sarebbe nil e avrei problemi nel numberOfItemsInSection, l'app crasherebbe
	var myHolidays = [HolidayDetail]() {
		didSet {
			print("riempio myHolidays")
			DispatchQueue.main.async {
				self.collectionView.reloadData()
				//self.navigationItem.title = "\(self.myHolidays.count) Holidays found"
				
				self.natHolidays = self.myHolidays.filter { $0.type.contains("National holiday")}
				self.locHolidays = self.myHolidays.filter { $0.type.contains("Local holiday")}
				self.relHolidays = self.myHolidays.filter { $0.type.contains("Season")}
				self.obsHolidays = self.myHolidays.filter { $0.type.contains("Observance")}
			}
		}
	}
	var array:[HolidayDetail]!
	var natHolidays = [HolidayDetail]()
	var locHolidays = [HolidayDetail]()
	var relHolidays = [HolidayDetail]()
	var obsHolidays = [HolidayDetail]()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		searchBar.delegate = self
		collectionView.delegate = self
		collectionView.dataSource = self
		searchBar.endEditing(true)
		navigationController?.navigationBar.barTintColor = UIColor.black
		navigationController?.navigationBar.barStyle = .black
		
		let holidayRequest = ApiRequest()
		holidayRequest.getResult { [weak self] result in
			switch result {
				case .failure(let error): print("Error1: \(error)")
				case .success(let holidays): print("Success")
				self?.myHolidays = holidays
			}
		}
		
		
		
	}
	
	

	
	//MARK: - Segment Control
	@IBAction func segControlClick(_ sender: Any) {
		collectionView.reloadData()
	}
	
	
	
	//MARK: - Collection View
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		switch segmentedControl.selectedSegmentIndex
		{
			case 0:
				return natHolidays.count
			case 1:
				return locHolidays.count
			case 2:
				return relHolidays.count
			case 3:
				return obsHolidays.count
			default:
				break
		}
		
		return 0 //mai eseguito
	}
	
	//creazione delle celle
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
		
		switch segmentedControl.selectedSegmentIndex
		{
			case 0:
				// scrollToItem()
				array=natHolidays
				self.navigationItem.title = "\(natHolidays.count) National Holidays"
			case 1:
				array=locHolidays
				self.navigationItem.title = "\(locHolidays.count) Local Holidays"
			case 2:
				array=relHolidays
				self.navigationItem.title = "\(relHolidays.count) Religious Holidays"
			case 3:
				array=obsHolidays
				self.navigationItem.title = "\(obsHolidays.count) Observance Holidays"
			
			default:
				break
		}
		
		cell.backgroundColor = UIColor.darkGray
		let holiday = array[indexPath.row]
		cell.titleLabel.text = holiday.name
		return cell
	}
	
	//prendo le informazioni della cella per passarli alla view di info
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		holidayTitle = array[indexPath.row].name
		holidayDescription = array[indexPath.row].description
		
		self.performSegue(withIdentifier: "mySegue", sender: self)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "mySegue"{
			if let vc = segue.destination as? InfoController {
				vc.holidayTitle = holidayTitle
				vc.holidayDescription = holidayDescription
			}
		}
	}
	
	//attributi della cella
	//Controlla il size inspector della collection view lo spazio tra celle
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
		
		let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
		let numberofItem: CGFloat = 3
		let collectionViewWidth = self.collectionView.bounds.width
		let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing
		let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left
		let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)
		
		return CGSize(width: width, height: 150)
	}
	
	//Quando clicco la search bar
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		//Devo deselezionarla subito altrimenti aprirò questa funzione all'infinito
		self.searchBar.endEditing(true)
		self.performSegue(withIdentifier: "mySegue2", sender: self)
	}

	
	

}

