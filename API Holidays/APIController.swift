//
//  APIController.swift
//  API Holidays
//
//  Created by Maurizio Minieri on 21/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import Foundation

enum HolidayError: Error {
	case noDataAvailable
	case canNotProcessData
}

struct ApiRequest {
	let resourceUrl:URL
	let API_KEY = "668fddb58d0eaaccfd5860fa56729321b07c386a"
	
	init(countryCode:String){ //costruttore con parametro country
		let date = Date()
		let format = DateFormatter()
		format.dateFormat = "yyyy"
		let currentYear = format.string(from: date)
		let resourceString="https://calendarific.com/api/v2/holidays?api_key=\(API_KEY)&country=\(countryCode)&year=\(currentYear)"
		
		guard let resourceURL = URL(string: resourceString) else {fatalError()}
		self.resourceUrl = resourceURL
	}
	
	init(){ //costruttore senza parametro
		let date = Date()
		let format = DateFormatter()
		format.dateFormat = "yyyy"
		let currentYear = format.string(from: date)
		let resourceString="https://calendarific.com/api/v2/holidays?api_key=\(API_KEY)&country=IT&year=\(currentYear)"
		guard let resourceURL = URL(string: resourceString) else {fatalError()}
		self.resourceUrl = resourceURL
	}
	
	func getResult(completion: @escaping(Result<[HolidayDetail],Error>) -> Void) {
		let dataTask = URLSession.shared.dataTask(with: resourceUrl) {data, response, error in
			guard let jsonData = data else {
				//completion(.failure(.noDataAvailable))
				return
			}
			
			let httpResponse = response as? HTTPURLResponse
			//httpResponse è un valore opzionale, quindi per stamparlo devo metterci il "?", per poterlo stampare solo per un valore non nil, oppure il "!" per chiudere l'esecuzione se il valore opzionale contiene nil
			print("Code: \(httpResponse!.statusCode)")
			
			//abbiamo il jsonData
			do{
				//print(jsonData)
				let decoder = JSONDecoder()
				let holidaysResponse = try decoder.decode(HolidayResponse.self, from: jsonData)
				let holidayDetails = holidaysResponse.response.holidays
				completion(.success(holidayDetails))
				//let httpResponse = response as? HTTPURLResponse
				//print("Code: \(httpResponse!.statusCode)")
			}catch{
				//completion(.failure(.canNotProcessData))
				print("Error: \(error)")
			}
		}
		dataTask.resume()
	}
	
}

