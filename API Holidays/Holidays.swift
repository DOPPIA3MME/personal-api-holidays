//
//  Holidays.swift
//  API Holidays
//
//  Created by Maurizio Minieri on 21/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import Foundation

struct HolidayResponse: Decodable{
	var response: Holidays
}

struct Holidays: Decodable {
	var holidays:[HolidayDetail]
}

struct HolidayDetail: Decodable {
	var name:String
	var date:DateInfo
	var type:[String]
	var description: String? //Se c'è il punto interrogativo sto dicendo al programma che questo campo potrebbe essere vuoto, cioè ci potrebbe essere null
}

struct DateInfo: Decodable {
	var iso:String
	var datetime:DateTimeDetail
}

struct DateTimeDetail: Decodable{
	var year:Int
	var month:Int
	var day:Int
}
