//
//  CollectionViewCell.swift
//  API Holidays
//
//  Created by Maurizio Minieri on 21/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var titleLabel: UILabel!
}
