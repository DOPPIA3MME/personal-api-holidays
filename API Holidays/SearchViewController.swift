//
//  SearchViewController.swift
//  API Holidays
//
//  Created by Maurizio Minieri on 21/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate  {
	
	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var collectionView: UICollectionView!
	
	//Se facessi una semplice dichiarazione myHolidays sarebbe nil e avrei problemi nel numberOfItemsInSection, l'app crasherebbe
	var myHolidays = [HolidayDetail]() {
		didSet {
			print("riempio myHolidays")
			DispatchQueue.main.async {
				self.collectionView.reloadData()
				self.navigationItem.title = "\(self.myHolidays.count) Holidays found"
				self.filteredData=self.myHolidays
			}
		}
	}
	
	var filteredData = [HolidayDetail]()
	var holidayTitle: String!
	var holidayDescription: String!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		print("SearchView caricata")
		searchBar.delegate = self
		collectionView.delegate = self
		collectionView.dataSource = self
		searchBar.becomeFirstResponder()
		let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
		textFieldInsideSearchBar?.textColor = .black
	}
	
	//MARK: - CollectionView
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		//print("numberOfItemsInSection: \(myHolidays.count)")
		//return myHolidays.count
		return filteredData.count
	}
	
	
	//creazione delle celle, la creazione dipende dallo scorrimento della collectionView
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
		
		cell.backgroundColor = UIColor.darkGray
		let holiday = filteredData[indexPath.row]
		cell.titleLabel.text = holiday.name
		
		return cell
	}
	
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		holidayTitle = filteredData[indexPath.row].name
		holidayDescription = filteredData[indexPath.row].description
		
		self.performSegue(withIdentifier: "mySegue", sender: self)
	}
	
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "mySegue"{
			if let vc = segue.destination as? InfoController {
				vc.holidayTitle = holidayTitle
				vc.holidayDescription = holidayDescription
			}
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
		
		let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
		let numberofItem: CGFloat = 1
		let collectionViewWidth = self.collectionView.bounds.width
		let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing
		let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left
		let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)
		return CGSize(width: width, height: 50)
	}
	
	//MARK: - SearchBar
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		
		filteredData = searchText.isEmpty ? myHolidays : myHolidays.filter { (item: HolidayDetail) -> Bool in
			// If dataItem matches the searchText, return true to include it
			return item.name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
		}
		
		collectionView.reloadData()
	}
	
	//ricerca avviata
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		guard let searchBarText = searchBar.text else {return}
	}
	
	//searcBar selezionata
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		searchBar.showsCancelButton = true
		let holidayRequest = ApiRequest()
		holidayRequest.getResult { [weak self] result in
			switch result {
				case .failure(let error): print("Error1: \(error)")
				case .success(let holidays): print("Success")
				self?.myHolidays = holidays
			}
		}
	}
	
	//cancelButton cliccato
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.text = nil
		searchBar.showsCancelButton = false
		searchBar.endEditing(true)
		filteredData = myHolidays
		collectionView.reloadData()
	}
}
