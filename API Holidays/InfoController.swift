//
//  InfoController.swift
//  API Holidays
//
//  Created by Maurizio Minieri on 21/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class InfoController: UIViewController {
	
	@IBOutlet weak var descriptionTextView: UITextView!
	
	var holidayTitle: String!
	var holidayDescription: String!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationItem.title = holidayTitle
		descriptionTextView.text = holidayDescription
	}
	
}
